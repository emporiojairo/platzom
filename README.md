# Plaztom

Platzom es un idioma inventado para el [curso de fundamentos de javascript](https://platzi.com/js) de Platzi

## Descripción del idioma

- Si la palabra termina con "ar", se le quitan esas dos letras
- Si la palabra inicia con Z, se le añade "pe" al final
- Si la palabra traducida tiene 10 o más letras, se debe partir en dos por la motad y unir con un guión medio
- Por ultimo, si la palabra original es un palíndromo, ninguna regla anterior cuenta y se devuelve la misma palabra pero intercalando letras mayúsculas y minúsculas

## Instalación

```
npm install platzom
```

##Uso

```
Import platzom from 'platzom'
platzom("Programar")// Program
platzom("Zorro")//Zorrope
platzom("Zarpar")//Zarppe
platzom("abecedario")//abece-dario
platzom("sometemos")//SoMeTeMoS
```

##Créditos
- [Jairo Raigosa](https://facebook.com/hiikiiro)

## Licencia

[MIT](https://opensource.org/licenses/MIT)
